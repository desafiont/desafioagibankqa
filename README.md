**Desafio Agibank**

Testes automatizados dos serviços disponibilizados pela API: https://jsonplaceholder.typicode.com/, validando os métodos GET, POST, PUT e DELETE.  

**Métodos utilizados:**

GET:  https://jsonplaceholder.typicode.com/posts;<br>
POST: https://jsonplaceholder.typicode.com/posts;<br>
PUT:  https://jsonplaceholder.typicode.com/posts; <br>
DELETE: https://jsonplaceholder.typicode.com/posts. <br>

**Recursos utilizados:**

• Rest-Assured; <br>
• Cucumber; <br>
• JUnit; <br>
• Java 8; <br>
• Gradle 6.6.1. 

**Como configurar o ambiente:**

• Faça clone do projeto: https://gitlab.com/desafiont/desafioagibankqa <br>
• Importe o projeto para sua IDE de preferência; <br>
• Faça a instalação na sua IDE do plugin 'Cucumber para Java'; <br>
• Necessário ter instalado o JDK8 

**Como executar a aplicação:**

• O projeto deve ser executado utilizando o comando: gradlew test --info <br>
• Para gerar o relatório executar o comando: gradle generateCucumberReports<br>
• O relatório do teste é gerado na pasta: reports/html-report<br>
• Abrir o arquivo reports/html-report/cucumber-html-reports/overview-features.html<br>

