package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarExcessoes {

    public RequestSpecification request;
    public Response response;

    @Given("que acessei GETID")
    public void que_acessei_GETID() throws Throwable {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("acessar ID no Posts")
    public void acessar_ID_no_Posts() throws Throwable {
        response = request.when().get("/posts/1").prettyPeek();
    }

    @Then("valido os dados no Posts")
    public void valido_os_dados_no_Posts() throws Throwable {
        Assert.assertEquals(response.jsonPath().getInt("id"), 1);
        Assert.assertEquals(response.jsonPath().getInt("userId"), 1);
        Assert.assertEquals(response.jsonPath().get("title"), "sunt aut facere repellat provident occaecati excepturi optio reprehenderit");

    }

}
