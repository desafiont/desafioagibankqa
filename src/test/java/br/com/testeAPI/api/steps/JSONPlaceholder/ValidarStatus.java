package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarStatus {

    public RequestSpecification request;
    public Response response;

    //ValidarStatusCode200 - GET
    @Given("que acessei a API")
    public void que_acessei_a_API() throws Throwable {
        request = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("acessar Posts")
    public void acessar_Posts() throws Throwable {
        response = request.when().get("/posts").prettyPeek();
    }

    @Then("valido o acesso")
    public void valido_o_acesso() throws Throwable {
        Assert.assertTrue(response.statusCode() == 200);
    }

    //ValidarStatusCode404 NOT FOUND - GET
    @When("acessar incorretamente")
    public void acessar_incorretamente() throws Throwable {
        response = request.when().get("/post").prettyPeek();
    }

    @Then("valido not found")
    public void valido_not_found() throws Throwable {
        Assert.assertTrue(response.statusCode() == 404);
    }

    //Validar Status 500 - POST
    @Given("que acesso esta incompleto")
    public void que_acesso_esta_incompleto() throws Throwable {
        request  = RestAssured.given().contentType("application/json; charset=UTF-8")
                .body("{title: 'foo', body: 'bar', userId: 1}").relaxedHTTPSValidation();
    }

    @When("acessar endereco")
    public void acessar_endereco() throws Throwable {
        response = request.when().post("/posts").prettyPeek();
    }

    @Then("valido status 500")
    public void valido_status() throws Throwable {
        Assert.assertTrue(response.statusCode() == 500);
    }

    //Validar Status 500 - PUT
    @When("acessar Put")
    public void acessar_Put() throws Throwable {
        response = request.when().put("/posts/1").prettyPeek();
    }
    //Validar Status 404 - DELETE
    @When("enviar Delete incorreto")
    public void enviar_Delete_incorreto() throws Throwable {
        response = request.when().delete("/posts").prettyPeek();
    }

}
