package br.com.testeAPI.api;

import org.junit.BeforeClass;

import static io.restassured.RestAssured.baseURI;

public class BaseTest {
    @BeforeClass
    public static void setup(){
      baseURI = Utils.lerDado("url.inicial");

    }
}
