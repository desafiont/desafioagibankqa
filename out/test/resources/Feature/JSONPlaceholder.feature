@JSONPlaceHolder
Feature: Validar fluxos e dados da API JSONPlaceholder

  @ValidarStatusCode200
  Scenario: Validar Status Code 200
    Given que acessei a API
    When acessar Posts
    Then valido o acesso

  @ValidarStatusCode404
  Scenario: Validar Status Code 404
    Given que acessei a API
    When acessar incorretamente
    Then valido not found

  @ValidarGetID1
  Scenario: Validar pesquisa por ID1
    Given que acessei GETID
    When acessar ID no Posts
    Then valido os dados no Posts

  @ValidarStatusCode500
  Scenario: Validar Status Code 500
    Given que acesso esta incompleto
    When acessar endereco
    Then valido status 500

  @ValidarAcessoIncorretoPut
  Scenario: Validar Acesso Incorreto Put
    Given que acesso esta incompleto
    When acessar Put
    Then valido status 500


  @ValidarPost
  Scenario: Validar inclusao de Posts
    Given que acessar Post
    When acessar tela inicial e incluir dados
    Then valido a inclusao

  @ValidarPut
  Scenario: Validar atualizacao
    Given que utilizei URL para Update
    When enviar dados para atualizacao
    Then validar atualizacao

  @ValidarDelete
  Scenario: Validar exclusao de Post
    Given que utilizei acesso para exclusao de Post
    When acessar tela inicial excluir dado inserido no Post
    Then valido a exclusao

  @ValidarAcessoIncorretoDelete
  Scenario: Validar Acesso Incorreto Delete - Nao informar Id
    Given que acessei a API
    When enviar Delete incorreto
    Then valido not found